import { Fragment } from 'react'
import ImageStripShort from '../components/imageStripShort'
import { TitleH3 } from '../components/Titles'

export default function GalleryOfWorks({ galleries, galleryMode, onSelect, showGalleryName = true }) {  
  return (
    <>
      <div className="gallery-of-works">
        {galleryMode === 'list' ? galleries.map(gallery => (
          <Fragment key={gallery.galleryKey}>
            {showGalleryName ? (
              <TitleH3 className="with-padding-x">{gallery.galleryName}</TitleH3>
            ) : ''}
            <div className="mb-2">
              <ImageStripShort imgs={gallery.galleryImgs} onSelect={(image) => { onSelect(image, gallery) }}></ImageStripShort>
            </div>
          </Fragment>
        )) : ''}
        {galleryMode === 'grid' ? (
          <div className="flex flex-row flex-wrap with-padding-x">
            {galleries.map(gallery => (
              gallery.galleryImgs.map(image => (
                <Fragment key={image.key}>
                  <button className="mr-3 mb-3" onClick={() => { onSelect(image.key, gallery) }}>
                    <div className={`relative h-24 w-32 lg:h-28 lg:w-40 cursor-pointer gallery-item`}>
                      <div className="h-24 rounded-xl w-32 lg:h-28 lg:w-40 block" alt={image.name} style={{backgroundImage: `url('${image.url}')`, backgroundSize: 'cover'}}></div>
                      <div className="item-name absolute bottom-0 p-2 w-32 lg:w-40 bg-brandPurple text-white text-left text-sm rounded-b-xl bg-opacity-80">{image.name}</div>
                    </div>
                  </button>
                </Fragment>
              ))
            ))}
          </div>
        ) : ''}
      </div>
      <style global jsx>{`
        .gallery-of-works .gallery-item .item-name  {
          display: none;
        }
        .gallery-of-works .gallery-item:hover .item-name  {
          display: block;
        }
      `}</style>
    </>
  )
}