import ScrollMenu from 'react-horizontal-scrolling-menu'

export default function ImageStripShort({ imgs, onSelect }) {
  // Menu
  const MenuItem = ({ text, url }) => {
    return <>
      <div className={`strip-menu-item z-0 h-24 w-32 lg:h-28 lg:w-40`}>
        <div className="h-24 rounded-xl w-32 lg:h-28 lg:w-40 block" alt={text} style={{backgroundImage: `url('${url}')`, backgroundSize: 'cover'}}></div>
        <div className="item-name absolute bottom-3 p-2 w-32 lg:w-40 bg-brandPurple text-white text-sm rounded-b-xl bg-opacity-80 whitespace-normal">{text}</div>
      </div>
    </>
  }

  const Menu = (list) => {
    const menuList = list.map(el => {
      const { key, name, url } = el
      return <MenuItem text={name} url={url} key={key} />
    })
    if (list.length) {
      menuList.push(<>
        <div className={`strip-menu-item z-0 h-24 w-32 opacity-0`}>
        </div>
      </>)
    }
    return menuList
  }

  // Arrow
  const ArrowLeft = <>
    <div className="relative w-0 z-10">
      <div className={'arrow-prev ml-4 text-white bg-brandPurple font-bold p-4 rounded-full w-6 h-6 absolute left-0 -top-4'}>
        <span className=" relative -top-3 -left-1.5">{'<'}</span>
      </div>
    </div>
  </>
  const ArrowRight = <>
    <div className="relative w-0 z-10">
      <div className={'arrow-next mr-4 text-white bg-brandPurple font-bold p-4 rounded-full w-6 h-6 absolute right-0 -top-4'}>
        <span className=" relative -top-3 right-1.5">{'>'}</span>
      </div>
    </div>
  </>

  // selection
  const menuList = Menu(imgs);

  return (
    <>
      <div className="image-strip-short">
        <ScrollMenu
          alignCenter={false}
          alignOnResize={false}
          arrowLeft={ArrowLeft}
          arrowRight={ArrowRight}
          clickWhenDrag={false}
          data={menuList}
          dragging={true}
          hideArrows={true}
          hideSingleArrow={true}
          // onFirstItemVisible={this.onFirstItemVisible}
          // onLastItemVisible={this.onLastItemVisible}
          onSelect={onSelect}
          // onUpdate={this.onUpdate}
          // ref={el => (this.menu = el)}
          scrollToSelected={false}
          // selected={selected}
          transition={+0.7}
          translate={0}
          wheel={true}
        />
      </div>

      <style global jsx>{`
        .image-strip-short .strip-menu-item {
          user-select: none;
          cursor: pointer;
          border: 1px transparent solid;
        }
        .image-strip-short .menu-item-wrapper {
          border: 1px transparent solid;
          margin: 5px;
        }
        .image-strip-short .menu-item-wrapper.active {
          
        }
        .image-strip-short .strip-menu-item.active {
          border: 1px green solid;
        }
        .image-strip-short .menu-item-wrapper .item-name  {
          display: none;
        }
        .image-strip-short .menu-item-wrapper:hover .item-name  {
          display: block;
        }

        .image-strip-short .scroll-menu-arrow {
          cursor: pointer;
          color: white;
        }
        
        .image-strip-short .scroll-menu-arrow--disabled {
          visibility: hidden;
        }

        .image-strip-short .menu-wrapper {
          padding: 5px 15px;
          width: 100%;
        }

      `}</style>
    </>
  )
}