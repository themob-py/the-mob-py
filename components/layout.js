import { useContext } from 'react'
import Head from 'next/head'
import Navbar from './navbar'
import NavbarFooter from './navbarFooter'
import { LayoutContext } from './layout.context'
import BackToTop from './backToTop'
import config from '../config'

export default function Layout({ children, home, title, withPadding, withPaddingY, fullMenuHeader }) {
  const pageTitle = title ? `${title} | ${config.siteTitle}` : config.siteTitle
  const { contentRef, isNavbarOpen, setIsNavbarOpen } = useContext(LayoutContext)

  let mainClasses = 'overflow-x-hidden w-sceen'
  if (withPaddingY) {
    mainClasses += ' pt-24 pb-20'
  }

  return (
    <body className="bg-gray-800 text-gray-400">
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>{pageTitle}</title>
        <meta
          name="description"
          content="Observatorio Covid-19 Al Sur"
        />
        {/* <meta
          property="og:image"
          content={`https://og-image.now.sh/${encodeURI(
            config.siteTitle
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        /> */}
        <meta name="og:title" content={config.siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <div id="top"></div>
      <Navbar home={home} fullMenuHeader={fullMenuHeader}></Navbar>
      <BackToTop></BackToTop>
      <button className={`fixed z-4000 w-full h-screen bg-black opacity-50 lg:hidden ${isNavbarOpen ? 'transition-opacity-full' : 'transition-opacity-none'}`} onClick={() => {setIsNavbarOpen(false)}} type="button"></button>
      <NavbarFooter></NavbarFooter>
      <main className={mainClasses} ref={contentRef}>{children}</main>
      <style global jsx>{`
        .z-5000 {
          z-index: 5000;
        }
        .z-4500 {
          z-index: 4500;
        }
        .z-4000 {
          z-index: 4000;
        }
        .z-2000 {
          z-index: 2000;
        }
      `}</style>
    </body>
  )
}