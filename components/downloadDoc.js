import React from 'react'
import Link from 'next/link'

export default function DownloadDoc() {
  return (
    <>
      <div className="md:max-w-md md:mx-auto">
        <div className="w-full overflow-x-hidden">
          <img className="place-items-center" src="/svg/bg-brush-top.svg" />
        </div>
        <div className="-mt-1 -mb-1 bg-brandPurple">
          <div className={`flex flex-row p-5 mx-5 bg-brandRed rounded-2xl place-content-center place-items-center`}>
            <div className="">
              <img className="w-14 ml-3" src="/svg/icon-download-white.svg" alt="" />
            </div>
            <div className="ml-8">
              <Link href={`/investigación.pdf`} target="blank">
                <a className="leading-tight download-link">
                  <span className="text-lg font-bold text-white uppercase border-brandPurple">Descarga</span>
                  <br />
                  <span className="border-b-2 text-rose-200 border-brandPurple">El documento de investigación aquí</span>
                </a>
              </Link>
            </div>
          </div>
        </div>
        <div className="w-full overflow-x-hidden">
          <img className="place-items-center" src="/svg/bg-brush-bottom.svg" />
        </div>
      </div>
      <style jsx>{`
        a.download-link span {
          border-bottom-width: 1px;
        }
        a.download-link:hover span {
          border-bottom: 1px solid white !important;
        }
      `}</style>
    </>
  )
}