import { useContext } from 'react'
import Link from 'next/link'
import { LayoutContext } from './layout.context'
import config from '../config'

export default function HomeHeader() {
  const { setScrollOnInit } = useContext(LayoutContext)

  return (
    <header id="header" className="flex flex-col">
      <div className="p-10 mb-6 mt-12 sm:mt-16 sm:mb-14 lg:hidden">
        <Link href={`/`}>
          <h1 className="flex flex-row items-center mx-auto cursor-pointer place-content-center" alt={config.siteTitle} title={config.siteTitle}>
            <div className="flex px-2">
              <img className=" w-40 sm:w-52 lg:w-40" src="/svg/logo-themob-white-red.svg"/>
            </div>
          </h1>
        </Link>
      </div>

      <style jsx>{`
        @media (min-width: 640px) {
          .home-footer-img {
            height: 470px;
          }
        }

        @media (min-width: 768px) {
          .home-footer-img {
            height: auto;
          }
        }
      `}</style>
    </header>
  )
}