
export default function GalleryTypeSelector({ galleryMode, onClick }) {  
  return (
    <div className="flex flex-row place-items-center cursor-pointer">
      <div className={`grid p-2 hover-action active-action ${galleryMode === 'list' ? 'active' : ''}`} onClick={() => {onClick('list')}}>
        <img className="w-4 default" src="/svg/icon-list-gray.svg" alt="Lista de imágenes"/>
        <img className="w-4 active" src="/svg/icon-list-purple.svg" alt="Lista de imágenes"/>
      </div>
      <div className={`grid p-2 hover-action active-action ${galleryMode === 'grid' ? 'active' : ''}`} onClick={() => {onClick('grid')}}>
        <img className="w-4 default" src="/svg/icon-grid-gray.svg" alt="Grilla de imágenes"/>
        <img className="w-4 active" src="/svg/icon-grid-purple.svg" alt="Grilla de imágenes"/>
      </div>
    </div>
  )
}