import React from 'react'
import { LayoutProvider } from './layout.context'

export default function WithLayout (WrappedPage) {
  const WithLayout = ({ ...pageProps }) => {
    return (
      <LayoutProvider>
        <WrappedPage {...pageProps} />
      </LayoutProvider>
    )
  }
  
  return WithLayout
}