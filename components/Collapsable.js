import React, { useState } from 'react'

export default function Collapsable ({ title, children }) {
  const [isCollapsed, setIsCollapsed] = useState(true)

  return (
    <div className="collapsable mb-5">
      <div className="collapse-title">
        <button className={`btn-text text-lg font-regular ${isCollapsed ? 'text-gray-500' : 'text-green-600'}`} onClick={() => { setIsCollapsed(!isCollapsed) }}>
          {title}
        </button>
      </div>
      {!isCollapsed ? (
        <div className="collapse-content font-regular p-2">
          {children}
        </div>
      ) : ''}
    </div>
  )
}
