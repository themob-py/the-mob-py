import ScrollMenu from 'react-horizontal-scrolling-menu'

export default function ImageStripLarge({ imgs, onSelect }) {
  // Menu
  const MenuItem = ({ text, url }) => {
    return <>
      <div className={`strip-menu-item z-0 h-52 w-32 lg:h-60 lg:w-36`}>
        <div className=" h-52 rounded-xl w-32 lg:h-60 lg:w-36 block" alt={text} style={{backgroundImage: `url('${url}')`, backgroundSize: 'cover'}}></div>
        <div className="absolute bottom-2.5 p-2 w-32 lg:w-36 bg-brandPurple text-white text-sm rounded-b-xl bg-opacity-80 whitespace-normal">{text}</div>
      </div>
    </>
  }

  const Menu = (list) => {
    const menuList = list.map(el => {
      const { key, name, url } = el
      return <MenuItem text={name} url={url} key={key} />
    })
    return menuList
  }

  // Arrow
  const ArrowLeft = <>
    <div className="relative w-0 z-10">
      <div className={'arrow-prev ml-4 text-white bg-brandPurple font-bold p-1 rounded-full w-6 h-6 absolute left-0 -top-2'}>
        <span className=" relative -top-1.5 left-0.5">{'<'}</span>
      </div>
    </div>
  </>
  const ArrowRight = <>
    <div className="relative w-0 z-10">
      <div className={'arrow-next mr-4 text-white bg-brandPurple font-bold p-1 rounded-full w-6 h-6 absolute right-0 -top-2'}>
        <span className=" relative -top-1.5 -right-0.5">{'>'}</span>
      </div>
    </div>
  </>

  // selection
  const menuList = Menu(imgs);

  return (
    <>
      <div className="image-strip-large">
        <ScrollMenu
          alignCenter={false}
          alignOnResize={false}
          arrowLeft={ArrowLeft}
          arrowRight={ArrowRight}
          clickWhenDrag={false}
          data={menuList}
          dragging={true}
          hideArrows={true}
          hideSingleArrow={true}
          // onFirstItemVisible={this.onFirstItemVisible}
          // onLastItemVisible={this.onLastItemVisible}
          onSelect={onSelect}
          // onUpdate={this.onUpdate}
          // ref={el => (this.menu = el)}
          scrollToSelected={false}
          // selected={selected}
          transition={+0.7}
          translate={0}
          wheel={true}
        />
      </div>

      <style global jsx>{`
        .image-strip-large .strip-menu-item {
          user-select: none;
          cursor: pointer;
          border: 1px transparent solid;
        }
        .image-strip-large .menu-item-wrapper {
          border: 1px transparent solid;
          margin: 5px;
        }
        .image-strip-large .menu-item-wrapper.active {
          
        }
        .image-strip-large .strip-menu-item.active {
          border: 1px green solid;
        }

        .image-strip-large .scroll-menu-arrow {
          cursor: pointer;
          color: white;
        }
        
        .image-strip-large .scroll-menu-arrow--disabled {
          visibility: hidden;
        }

        
        .image-strip-large .menu-item-wrapper:last-child {
          margin-right: 200px;
        }
        .image-strip-large .menu-wrapper {
          padding: 20px 15px;
          width: 100%;
        }
      `}</style>
    </>
  )
}