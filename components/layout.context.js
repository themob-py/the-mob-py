import { useEffect, useState, useRef, createContext } from 'react'
import { animateScroll } from 'react-scroll'
import { useRouter } from 'next/router'

export const LayoutContext = createContext({
  contentRef: null,
  isNavbarOpen: false,
  setIsNavbarOpen: () => null,
  isScrollUnderOffset: false,
  setScrollUnderOffset: () => null,
  scrollPosition: false,
  setScrollPosition: () => null,
  scrollOnInit: false,
  setScrollOnInit: () => null,
  innerViewPort: false,
  setInnerViewPort: () => null
})

export const LayoutProvider = ({ children }) => {
  const contentRef = useRef(null)
  const [isNavbarOpen, setIsNavbarOpen] = useState(false)
  const [isScrollUnderOffset, setScrollUnderOffset] = useState(false)
  const [scrollPosition, setScrollPosition] = useState(0)
  const [scrollOnInit, setScrollOnInit] = useState(null)
  const [innerViewPort, setInnerViewPort] = useState({})
  const router = useRouter()

  // show full navbar
  const topOffset = 250
  const handleScroll = () => {
    setScrollUnderOffset(window.pageYOffset >= topOffset)
    setScrollPosition(window.pageYOffset)
  }
  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', () => handleScroll)
    }
  }, [])

  // inner view port
  // show full navbar
  const handleInnerViewPort = () => {
    const headerMenu = document.getElementById('header-menu')
    const footerMenu = document.getElementById('footer-menu')
    setInnerViewPort({
      headerMenuHeight: headerMenu.clientHeight,
      footerMenuHeight: footerMenu.clientHeight,
      windowHeight: window.innerHeight,
      innerHeight: window.innerHeight - headerMenu.clientHeight - footerMenu.clientHeight
    })
  }
  useEffect(() => {
    window.addEventListener('resize', handleInnerViewPort)
    return () => {
      window.removeEventListener('resize', () => handleInnerViewPort)
    }
  }, [])

  // scroll on init
  useEffect(() => {
    const handleRouteChange = () => {
      let scrollTo = scrollOnInit || 'top'
      if (process.browser && scrollTo) {
        scrollTo && setScrollOnInit(null)
        const elementToScroll = document.getElementById(scrollTo)
        if (elementToScroll) {
          animateScroll.scrollTo(elementToScroll.offsetTop, {
            duration: 500,
            delay: 0,
            smooth: true,
            offset: 0
          })
        }
      }
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [scrollOnInit])

  return <LayoutContext.Provider value={{ 
    contentRef,
    isNavbarOpen,
    setIsNavbarOpen,
    isScrollUnderOffset,
    setScrollUnderOffset,
    scrollPosition,
    setScrollPosition,
    scrollOnInit,
    setScrollOnInit,
    innerViewPort,
    setInnerViewPort,
    handleInnerViewPort
  }}>{children}</LayoutContext.Provider>
}