import { useContext } from 'react'
import Link from 'next/link'
import { Link as LinkScroll } from 'react-scroll'
import ActiveLink from './activeLink'
import { LayoutContext } from './layout.context'

export default function NavbarHome({ home, fullMenuHeader }) {
  const { isNavbarOpen, isScrollUnderOffset, setIsNavbarOpen, setScrollOnInit } = useContext(LayoutContext)
  
  const showFullHeader = () => fullMenuHeader || isNavbarOpen || isScrollUnderOffset

  const menuItems = (
    <div className="px-4 font-semibold">
      {home ? (
        <LinkScroll to="header" smooth={true} offset={0} duration={800} href="" className={`block mt-1 active lg:inline lg:mt-0 menu-btn font-regular`}>
          Inicio
        </LinkScroll>
      ) : (
        <ActiveLink href={`/`} activeClass="active" scroll={false}>
          <a onClick={() => setScrollOnInit('header')} className={`block lg:inline lg:mt-0 menu-btn`}>
            Inicio
          </a>
        </ActiveLink>
      )}
      <ActiveLink href={`/Menu`} scroll={false} activeClass="active">
        <a onClick={() => setScrollOnInit('content')} className={`block mt-1 lg:inline lg:mt-0 lg:ml-2 menu-btn`}>
          Menú
        </a>
      </ActiveLink>
      {/* mostrar sólo si hay algo en el carrito */}
      <ActiveLink href={`/HacerPedido`} scroll={false} activeClass="active">
        <a onClick={() => setScrollOnInit('content')} className={`block mt-1 lg:inline lg:mt-0 lg:ml-2 menu-btn`}>
          Hacer pedido TODO
        </a>
      </ActiveLink>
    </div>
  )
  
  return (
    <>
      <header id="header-menu" className={`z-5000 sm:flex sm:h-14 sm:px-4 sm:py-2 w-full fixed top-0 left-0 ${showFullHeader() ? 'shadow-lg bg-gray-800' : ''}`}>
        <div className="flex w-full px-4 py-5 sm:p-0 place-content-between place-items-center">
          {/* navbar header */}
          <div className={`z-5000 ${showFullHeader() ? 'transition-opacity-full' : 'transition-opacity-none'}`}>
            {/* logo link */}
            <Link href={`/`} scroll={false}>
              <a onClick={() => setScrollOnInit('header')} className="flex flex-row mx-auto place-items-center" title={''}>
                <div className="flex w-9 mr-2">
                  <img src="/svg/logo-themob-white-red.svg" className="" />
                </div>
              </a>
            </Link>
          </div>
          {/* navbar movile trigger */}
          <div className="lg:hidden">
            <button onClick={() => setIsNavbarOpen(!isNavbarOpen)} className="block p-2 w-8 text-center text-gray-500 focus:text-gray-700 focus:outline-none" type="button">
              <div className="w-full mx-auto h-auto sm:w-4">
                {isNavbarOpen && <img src="/svg/icon-close-white.svg" />}
                {!isNavbarOpen && <img src={`${home ? ('/svg/icon-menu-purple.svg') : ('/svg/icon-menu-white.svg')}`} />}
              </div>
            </button>
          </div>

          {/* navbar content desktop */}
          <div className={`hidden lg:flex bg-white p-1.5 rounded-full ${(!isScrollUnderOffset && !isNavbarOpen) ? 'shadow-lg' : ''}`}>
            {menuItems}
          </div>
        </div>
      </header>

      {/* navbar content mobile */}
      <div className={`z-4500 fixed top-0 left-0 w-4/5 sm:w-3/4 md:w-3/5 h-screen bg-white overflow-y-scroll pt-32 pb-4 sm:pt-20 lg:hidden ${isNavbarOpen ? 'block visible' : 'hidden invisible'}`}>
        <div className="mb-6">
          {menuItems}
        </div>
      </div>
    </>
  )
}