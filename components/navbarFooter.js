import { useContext } from 'react'
import ActiveLink from './activeLink'
import { LayoutContext } from './layout.context'

export default function NavbarFooter({}) {
  const { setScrollOnInit } = useContext(LayoutContext)
  
  return (
    <>
      <div id="footer-menu" className="z-2000 bg-gray-900 p-1.5 fixed bottom-0 w-full flex flex-row place-content-center flex-nowrap">
        <ActiveLink href={`/page`} scroll={false} activeClass="active">
          <a onClick={() => setScrollOnInit('content')} className={`flex w-12 sm:w-14 mx-4 footer-menu-btn active-action`} title="Mapa">
            <img className="w-12 default" src="/svg/icon-map-gray.svg"/>
            <img className="w-12 active" src="/svg/icon-map-white.svg"/>
          </a>
        </ActiveLink>
      </div>
    </>
  )
}