import { useContext } from 'react'
import { Link as LinkScroll } from 'react-scroll'
import {
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar"
import "react-circular-progressbar/dist/styles.css"
import { LayoutContext } from './layout.context'

export default function BackToTop() {
  const { isScrollUnderOffset, scrollPosition } = useContext(LayoutContext)
  const getScrollPercentage = () => {
    if (process.browser) {
      return (scrollPosition + window.innerHeight) * 100 / document.documentElement.offsetHeight
    }
  }
  return (
    <LinkScroll to="top" smooth={true} offset={0} duration={800}>
      <button type="button" className={`z-2000 fixed p-0.5 bg-white rounded-full w-14 h-14 bottom-20 sm:bottom-24 right-7 shadow-xl ${isScrollUnderOffset ? 'transition-opacity-90' : 'transition-opacity-none'}`}>
        <CircularProgressbarWithChildren value={getScrollPercentage()} styles={buildStyles({
          pathColor: "#95006A",
          trailColor: "white"
        })}>
          <div className="w-5">
            <img src="/svg/icon-go-to-top.svg" />
          </div>
        </CircularProgressbarWithChildren>
      </button>
    </LinkScroll>
  )
}