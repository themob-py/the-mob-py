
export function TitleMain({ children }) {
  return (
    <>  
      <h1 className="font-bold text-3xl mb-4">
        <div className="w-full overflow-x-hidden">
          <img className="place-items-center" src="/svg/bg-brush-top.svg" />
        </div>
        <div className="-mt-1 -mb-1 bg-brandPurple leading-none text-center md:flex-row">
          <div className="relative -top-2 px-4 pt-3 text-white">
            <span>{children}</span>
          </div>
        </div>
        <div className="w-full overflow-x-hidden">
          <img className="place-items-center" src="/svg/bg-brush-bottom.svg" />
        </div>
      </h1>
    </>
  )
}

export function TitleH1({ children, className }) {
  return (
    <>  
      <h1 className={`font-bold text-3xl mb-4 lg:text-4xl xl:text-5xl ${className}`}>
        <img src="/svg/icon-bullet.svg" className="w-4 -mt-1 mr-1.5 inline lg:w-5 xl:w-6"/>
        <span>{children}</span>
      </h1>
    </>
  )
}

export function SubtitleH1({ children }) {
  return (
    <>  
      <>  
      <span className="font-medium text-2xl mb-4">
        <img src="/svg/icon-bullet.svg" className="w-4 -mt-1 mr-1.5 inline"/>
        <span className="text-gray-500">{children}</span>
      </span>
    </>
    </>
  )
}

export function TitleH2({ children, className }) {
  return (
    <>  
      <h2 className={`font-semibold text-2xl mb-3 md:text-3xl ${className}`}>
        <span>{children}</span>
      </h2>
    </>
  )
}

export function SubtitleH2({ children }) {
  return (
    <>  
      <>  
      <span className="font-medium text-lg mb-4">
        <img src="/svg/icon-bullet.svg" className="w-3 -mt-1 mr-1.5 inline"/>
        <span className="text-gray-500">{children}</span>
      </span>
    </>
    </>
  )
}

export function TitleH3({ children, className }) {
  return (
    <>  
      <h2 className={`font-semibold text-xl mb-3 md:text-2xl xl:text-3xl ${className}`}>
        <span>{children}</span>
      </h2>
    </>
  )
}