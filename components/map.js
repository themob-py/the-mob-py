import { MapContainer, TileLayer } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css'
import "leaflet-defaulticon-compatibility"
import MyPositionBtn from '../lib/mapUtils/myPosition'
import MapMarker from '../lib/mapUtils/MapMarker'
import { TitleH2, SubtitleH2 } from '../components/Titles'
import { useRouter } from 'next/router'

export default function MapMain({ className, mapCenter, zoom=11,  worksList=[], showImgs=true }) {
  const router = useRouter()
  const defaultLocation = [-25.328085331372463, -57.52879463600459]
  const onSelectArtist = (artistKey) => {
    if (artistKey) {
      router.push(`/artista/${artistKey}`)
    }
  }
  const onSelectWork = (workKey) => {
    if (workKey) {
      router.push(`/mural/${workKey}`)
    }
  }

  return (
    <MapContainer center={mapCenter || defaultLocation} zoom={zoom} scrollWheelZoom={true} className={className}>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <MyPositionBtn></MyPositionBtn>
      <MapMarker key={'main-marker'} iconUrl="/svg/icon-map-purple.svg" position={mapCenter || defaultLocation}>
        <div className=" w-48 sm:w-56">
          {/* {showImgs ? (
            <img className="block object-cover w-48 sm:w-56 max-h-72 mb-3 pt-1 rounded-lg shadow-md" src={`/img/thumbs/${artist.artistKey}/${work.workImgs[0]}`} alt={work.name}/>
          ) : ''} */}
          {/* <a onClick={() => onSelectWork(work.key)} className="block cursor-pointer" title="Mural">
            <TitleH2 className="text-brandPurple">{work.name}</TitleH2>
          </a>
          <a onClick={() => onSelectArtist(artist.artistKey)} className="block cursor-pointer -mt-3 mb-3" title="Artista"> 
            <SubtitleH2>{artist.artistName}</SubtitleH2>
          </a> */}
          {/* <span className="block font-regular text-base leading-tight">{work.desc}</span> */}
          {/* <span className="block font-regular text-sm mt-2 text-gray-600">{work.address}</span> */}
          <span className="block font-bold text-lg text-gray-900">The MOB</span> 
        </div>
      </MapMarker> 
    </MapContainer> 
  )
}