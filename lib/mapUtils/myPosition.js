import { useState } from 'react'
import { useMap, useMapEvents } from 'react-leaflet'
import Control from './mapControl'
import MapMarker from './MapMarker'

export default function MyPositionBtn({  }) {
  let [ myPosition, setMyPosition ] = useState(null)

  const map = useMap()
  const mapEvents = useMapEvents({
    locationfound: (location) => {
      const myPosition = [location.latitude, location.longitude]
      map.flyTo(myPosition, 16)
      setMyPosition(myPosition)
    }
  })

  const goToPosition = () => {
    map.locate()
  }

  return (
    <>
      {/* my position */}
      {myPosition ? (
        <MapMarker iconUrl="/svg/icon-gps-position-purple.svg" position={myPosition}>
          Mi posición
        </MapMarker>
      ) : ''}
      
      <Control position="topright" >
        <button
          className="bg-warmGray-50 border-gray-700 rounded-sm p-2 hover:bg-warmGray-200"
          onClick={goToPosition}
        >
          <span className="font-bold">Mi ubicación</span> <img src="/svg/icon-gps-position-black.svg" className="w-4 h-4 inline-block"/>
        </button>
      </Control>
    </>
  )
}