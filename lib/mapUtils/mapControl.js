/**
 * @see https://react-leaflet.js.org/docs/example-react-control
 */

// Classes used by Leaflet to position controls.
export const POSITION_CLASSES = {
    bottomleft: 'leaflet-bottom leaflet-left',
    bottomright: 'leaflet-bottom leaflet-right',
    topleft: 'leaflet-top leaflet-left',
    topright: 'leaflet-top leaflet-right',
};

const MapCustomControl = ({ position, containerProps, children }) => {
    return (
        <div className={POSITION_CLASSES[position]}>
            <div className='leaflet-control leaflet-bar' {...containerProps}>
                {children}
            </div>
        </div>
    );
};

MapCustomControl.defaultProps = {
    position: 'topleft',
    containerProps: {},
    children: null,
};

export default MapCustomControl;