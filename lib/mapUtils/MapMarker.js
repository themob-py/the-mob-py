import { Marker, Popup } from 'react-leaflet'
import L from 'leaflet'

export default function MapMarker({ iconUrl, children, position }) {
  const getIcon = () => {
    return L.icon({
      iconUrl: iconUrl,
      iconRetinaUrl: iconUrl,
      iconAnchor: null,
      popupAnchor: [0, -20],
      shadowUrl: null,
      shadowSize: null,
      shadowAnchor: null,
      iconSize: [40, 40],
      className: ''
    })
  }

  return (
    <Marker position={position} icon={getIcon()}>
      <Popup>
        {children}
      </Popup>
    </Marker>
  )
}