const sharp = require('sharp')

// Callback FN
module.exports.resizeImage = function ({
  original, // String
  dest, // String
  width, // Number
  height, // Number
  quality // Number
}, cb) {
  console.log('original', original)
  sharp(original)
    .resize({
      width,
      height,
      kernel: sharp.kernel.nearest,
      fit: 'cover',
      // position: 'center',
      // gravity: 'center'
    })
    .jpeg({ quality })
    .toFile(dest, cb)
}