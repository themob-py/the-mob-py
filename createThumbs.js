const imageUtils = require('./lib/imageUtils')
const data = require('./data/map')
const fs = require('fs')
const path = require('path')

let originalFolder = './data/'
let exportedFolder = './public/img'
let fullFolder = exportedFolder + '/full'
let thumbsFolder = exportedFolder + '/thumbs'
mkdirSync(exportedFolder)
mkdirSync(fullFolder)
mkdirSync(thumbsFolder)
const dataMap = data.getThumbs({ withWorks: true })

// Artist images
dataMap.map(artistData => {
  // Main thumb
  resize({
    key: artistData.artistKey,
    destFolder: thumbsFolder,
    fileUri: `${artistData.artistKey}/${artistData.artistImg}`,
    width: 360,
    height: 640,
    quality: 50
  })

  artistData.work.map(work => {
    work.workImgs.map(workUri => {
      // Full
      resize({
        key: artistData.artistKey,
        destFolder: fullFolder,
        fileUri: `${artistData.artistKey}/${workUri}`,
        quality: 80
      })
      // Thumbs
      resize({
        key: artistData.artistKey,
        destFolder: thumbsFolder,
        fileUri: `${artistData.artistKey}/${workUri}`,
        width: 360,
        height: 240,
        quality: 50
      })
    })
  })
})


function resize({
  key, // String
  fileUri, // String
  destFolder, // String
  width, // Number
  height, // Number
  quality // Number
}) {
  mkdirSync(path.resolve(destFolder, key))
  imageUtils.resizeImage({
    original: path.resolve(originalFolder, fileUri),
    dest: path.resolve(destFolder, fileUri),
    width,
    height,
    quality
  }, (err, res) => {
    if (err) {
      console.error('\n\nerr', err, '\n\n DATA: ', {
        original: path.resolve(originalFolder, fileUri),
        dest: path.resolve(destFolder, fileUri),
        width,
        height,
        quality
      })
      throw Error(err)
    }
    // console.log('res', res)
  })
}



function mkdirSync(path) {
  try {
    fs.mkdirSync(path);
  } catch (e) {
    if (e.code != 'EEXIST') throw e;
  }
}
