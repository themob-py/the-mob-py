import Layout from '../components/layout'

export default function HacerPedido() {
  return (
    <Layout fullMenuHeader withPaddingY title="Hacer pedido">
      <section className="p-2">
        <h1>Hacer pedido</h1>
      </section>
    </Layout>
  )
}

export async function getStaticProps(props) {
  return {
    props: {
    }
  }
}