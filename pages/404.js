import Layout from '../components/layout'
import { TitleH1 } from '../components/Titles'

export default function Custom404() {
  return (
    <Layout fullMenuHeader withPadding title='Página no encontrada - Error 404'>
      <section>
        <TitleH1>Página no encontrada - Error 404</TitleH1>
      </section>
    </Layout>
  )
}

export async function getStaticProps(props) {
  return {
    props: {
    }
  }
}