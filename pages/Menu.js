import Layout from '../components/layout'
import Collapsable from '../components/Collapsable'

export default function Menu() {
  const categories = [{
    key: 'bebidas',
    name: 'Bebidas'
  }, {
    key: 'parrilla',
    name: 'Parrilla'
  }, {
    key: 'buffet',
    name: 'Buffet'
  }, {
    key: 'entradas',
    name: 'Entradas'
  }, {
    key: 'pizzas',
    name: 'Pizzas'
  }]

  const productList = [{
    nombre: 'Coca cola',
    precio: 9000,
    type: 'normal', // 'pizza'
    categoria: 'bebidas'
  }, {
    nombre: 'Coca cola',
    precio: 9000,
    type: 'normal', // 'pizza'
    categoria: 'bebidas'
  }, {
    nombre: 'Coca cola',
    precio: 9000,
    type: 'normal', // 'pizza'
    categoria: 'bebidas'
  }, {
    nombre: '',
    precio: 9000,
    type: 'normal', // 'pizza'
    categoria: 'pizzas'
  }, {
    nombre: 'Lomito',
    precio: 35000,
    type: 'normal', // 'pizza'
    categoria: 'parrilla'
  }, {
    nombre: 'Pancho',
    precio: 9000,
    type: 'normal', // 'pizza'
    categoria: 'parrilla'
  }];

  return (
    <Layout fullMenuHeader withPaddingY title="Menú">
      <section className="p-2">
        <h1 className="text-lg font-black">Menú</h1>

        <div className="mt-5">
          {categories.map(({ key, name }) => (
            <Collapsable title={name} key={key}>
              {name}
              <div className="w-100 flex flex-col">

                {productList
                  .filter(({ categoria }) => (categoria === key))
                  .map(({ nombre, precio }) => (
                    <div className="flex flex-row">
                      <div className="">{nombre}</div>
                      <div className="">{precio.toString()} Gs.</div>
                    </div>
                  ))}
              </div>
            </Collapsable>
          ))}
        </div>
      </section>
    </Layout>
  )
}

export async function getStaticProps(props) {
  return {
    props: {
    }
  }
}